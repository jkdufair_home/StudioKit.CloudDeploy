﻿<#
.SYNOPSIS
This script will deploy assets in a subfolder named "build" to blob storage

.DESCRIPTION
All folders under "build" need a corresponding blob container with the same name. All files in each subfolder will be uploaded to said blob container. This script requires that you have a .publishsettings file for your subscription located in the c:\ directory (This directory was chosen because it is known to exist on both CI build agents and on local dev machines)

.EXAMPLE
./Deploy-Assets -StorageAccountName "forecastdevcontent" -Subscription "dev.academicforecast.org" -PublishSettingsFile "azure-dev"
#>

Param(
	[Parameter(Mandatory=$true)][string]$StorageAccountName,
	[Parameter(Mandatory=$true)][string]$Subscription,
	[Parameter(Mandatory=$true)][string]$PublishSettingsFile
)

$ErrorActionPreference = "Stop"

$currentLocation = (Get-Location).Path
Write-Output "$(Get-Date -f $timeStampFormat) Importing publish settings file from '$PublishSettingsFile'"
$publishSettings = Import-AzurePublishSettingsFile $PublishSettingsFile

Write-Output "$(Get-Date -f $timeStampFormat) Setting Subscription to '$Subscription'"
Select-AzureSubscription -SubscriptionName $Subscription

Write-Output "$(Get-Date -f $timeStampFormat) Setting StorageAccount to '$StorageAccountName'"
Set-AzureSubscription -CurrentStorageAccount $StorageAccountName -SubscriptionName $Subscription

$storageAccountKey = (Get-AzureStorageKey -StorageAccountName $StorageAccountName).Primary
$context = New-AzureStorageContext -StorageAccountName $StorageAccountName -StorageAccountKey $StorageAccountKey
$container = "static"

Write-Output "$(Get-Date -f $timeStampFormat) Deleting old blobs in Container '$container'"
$blobs = Get-AzureStorageBlob -Container $container -Context $content
$blobs | Remove-AzureStorageBlob

Write-Output "$(Get-Date -f $timeStampFormat) Copying build files to Container '$container'"
$files = Get-ChildItem "build" -Name -File -Recurse -Exclude *.html
foreach($file in $files)
{
	$fileName="$currentLocation\build\$file"
	$blobName = $file
	if ($file -match "$container\\") {
		$blobName = $file -replace "^$container\\",""
	}
	$extension = [System.IO.Path]::GetExtension($file)
	$contentType = switch ($extension)
	{
		.js { "application/javascript" }
		.json { "application/json" }
		.ttf { "application/font-sfnt" }
		.otf { "application/font-sfnt" }
		.woff { "application/font-woff" }
		.woff2 { "application/font-woff2" }
		.eot { "application/vnd.ms-fontobject" }
		.svg { "image/svg+xml" }
		.ico { "image/vnd.microsoft.icon" }
		.jpg { "image/jpeg" }
		.png { "image/png" }
		.gif { "image/gif" }
		.css { "text/css" }

		default { "application/octet-stream" }
	}
	Write-Output "Copying '$fileName' to Container '$container' Blob '$blobName' ContentType '$contentType'"
	$result = Set-AzureStorageBlobContent -File $filename -Container $container -Blob $blobName -Context $context -Force -Properties @{"ContentType" = $contentType}
}
Write-Host "All files uploaded"
