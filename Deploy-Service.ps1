<#
.SYNOPSIS
This script will deploy an Azure cloud service

.DESCRIPTION
This script is designed to be run from the directory containing the solution file.

.EXAMPLE
./Deploy-Service -Subscription "dev.academicforecast.org" -StorageAccount "forecastdevstorage" -Service "forecast-dev" -Slot "Production" -ProjectName "Forecast.Azure" -BuildConfiguraton "Release" -TargetProfile "Development" -PublishSettingsFile "c:\azure-dev.publishsettings"
#>
param(
    [Parameter(Mandatory=$true)][string]$Subscription,
    [Parameter(Mandatory=$true)][string]$StorageAccount,
    [Parameter(Mandatory=$true)][string]$Service,
    [Parameter(Mandatory=$true)][string]$Slot,
    [Parameter(Mandatory=$true)][string]$ProjectName,
    [Parameter(Mandatory=$true)][string]$BuildConfiguration,
    [Parameter(Mandatory=$true)][string]$TargetProfile,
    [Parameter(Mandatory=$true)][string]$PublishSettingsFile
)

$currentLocation = (Get-Location).Path
$package = "$currentLocation\$ProjectName\bin\$BuildConfiguration\app.publish\$ProjectName.cspkg"
$BuildConfigurationFile = "$currentLocation\$ProjectName\bin\$BuildConfiguration\app.publish\ServiceConfiguration.$TargetProfile.cscfg"
$timeStampFormat = "g"
$deploymentLabel = "TeamCity deploy to $Service v$env:build_number"

Write-Output "$(Get-Date -f $timeStampFormat) Importing publish settings file"
$publishSettings = Import-AzurePublishSettingsFile $PublishSettingsFile
Select-AzureSubscription $Subscription

Write-Output "$(Get-Date -f $timeStampFormat) Setting subscription"
Set-AzureSubscription -CurrentStorageAccount $StorageAccount -SubscriptionName $Subscription

$existingDeployment = Get-AzureDeployment -ServiceName $Service -Slot $Slot -ErrorAction silentlycontinue
if ($existingDeployment.Name -eq $null)
{
    Write-Output "$(Get-Date -f $timeStampFormat) Creating deployment"
    $setDeployment = New-AzureDeployment -Slot $Slot -Package $package -Configuration $BuildConfigurationFile -label $deploymentLabel -ServiceName $Service
} else {
    Write-Output "$(Get-Date -f $timeStampFormat) Updating deployment"
    $setdeployment = Set-AzureDeployment -Upgrade -Slot $Slot -Package $package -Configuration $BuildConfigurationFile -label $deploymentLabel -ServiceName $Service -Force
}

Write-Output "$(Get-Date -f $timeStampFormat) Completing deployment"
$completeDeployment = Get-AzureDeployment -ServiceName $Service -Slot $Slot
$completeDeploymentID = $completeDeployment.deploymentid
 
Write-Output "$(Get-Date -f $timeStampFormat) Deployment complete. Deployment ID: $completeDeploymentID"
